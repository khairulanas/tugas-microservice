#!/bin/bash

#get server list
set -f 
string=$DEPLOY_SERVER
array=(${string//,/ })

#iterate server for deploy and commit
for i in "${!array[@]}" do
  echo "deploy project on server ${array[i]}"
  ssh ubuntu@${array[i]} "cd /var/www && git pull origin master"
done
